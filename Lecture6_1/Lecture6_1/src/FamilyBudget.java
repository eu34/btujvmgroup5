import java.io.FileWriter;
import java.io.IOException;

public class FamilyBudget {
    private int money;

    public void setMoney(int money){
        this.money = money;
    }

    public int getMoney(){
        return money;
    }

    public void changeMoney(int amount){
        this.money += amount;
        writeStateOfMoney();
    }

    public void writeStateOfMoney(){
        try {
            FileWriter writer = new FileWriter("money.txt", true);
            writer.write(this.money+"\n");
            writer.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
