import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextArea;

import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;

public class ViewOfGenerator implements Initializable {

    @FXML
    public ComboBox alphabet;
    @FXML
    public Spinner numberOfSentences;
    @FXML
    public TextArea result;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        alphabet.setValue("Georgian");
        alphabet.getItems().add("Georgian");
        alphabet.getItems().add("English");
    }

    private String abcGeo[] = {"ლჯკდასკუს ასდფლჰნ ასლფჰნკუ", "ასკფჯ აკსჯჰ ასკფუჰ", "კჯჰასფ  იოასჰდფ იუ"};
    private String abcEn[] = {"sdkjh sdkfh sd jsdhf khf ksk", "dsjkh jskldh ", "kjhfsd ksdjhf sdf sdkjhf ksd"};

    public void generate(){
        String abc = alphabet.getValue().toString();
        int numberOfS = (int) numberOfSentences.getValue();
        String randomText = "";
        Random r = new Random();
        if(abc.equals("Georgian")) {
            for (int i = 0; i < numberOfS; i++) {
                randomText += abcGeo[r.nextInt(0, abcGeo.length)]+ "\n";
            }
        }else if(abc.equals("English")){
            for (int i = 0; i < numberOfS; i++) {
                randomText += abcEn[r.nextInt(0, abcEn.length)]+ "\n";
            }
        }
        result.setText(randomText);
    }

    public void clear(){
        result.clear();
    }
}
