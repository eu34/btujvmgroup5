package files;

import java.io.*;

public class TestFiles {
    public  void createFile1(){
        try {
            FileWriter writer = new FileWriter("file1.txt", true);
            writer.write("\nJava");
            writer.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void createFile2(){
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("c:/jvm/file2.txt"));
            writer.write("hello Java");
            writer.close();
        } catch (FileNotFoundException e){
            System.out.println(e.getMessage());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void readFile(){
        try {
            FileReader reader = new FileReader("file1.txt");
            System.out.println((char) reader.read());
            reader.close();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    public void readFile1(){
        try {
            BufferedReader reader = new BufferedReader(new FileReader("file1.txt"));
            System.out.println(reader.readLine());
            reader.close();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
