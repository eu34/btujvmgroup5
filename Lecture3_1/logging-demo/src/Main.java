import clockit.model.Employ;
import clockit.service.ClockItService;
import clockit.service.impl.ClockItServiceImpl;
import org.apache.log4j.Logger;

public class Main {

    final static Logger logger=Logger.getLogger(Main.class);

    public static void main(String[] args) {

        Employ Zura=new Employ("Zura",true);
        Employ Gio=new Employ("Gio",false);
        Employ Ani=new Employ("Ani",true);
        Employ Mari=new Employ("",true);


        ClockItService clockItService=new ClockItServiceImpl();
        clockItService.checkIn(Zura);
        clockItService.checkIn(Gio);
        clockItService.checkIn(Ani);
        clockItService.checkIn(Mari);

    }
}