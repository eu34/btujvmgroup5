package clockit.model;


public class Employ {
    private String name;
    private boolean permission;


    public Employ() {
    }

    public Employ(String name, boolean permission) {
        this.name = name;
        this.permission = permission;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isPermission() {
        return permission;
    }

    public void setPermission(boolean permission) {
        this.permission = permission;
    }

    @Override
    public String toString() {
        return "Employ{" +
                "name='" + name + '\'' +
                ", permission=" + permission +
                '}';
    }
}
