package clockit.service.impl;

import clockit.model.Employ;
import clockit.service.ClockItService;
import org.apache.log4j.Logger;

import java.util.Date;

public class ClockItServiceImpl implements ClockItService {
    final static Logger logger=Logger.getLogger(ClockItServiceImpl.class);

    @Override
    public void checkIn(Employ employ) {
        if (employ.getName().isEmpty()){
            try{
                throw new ArithmeticException();
            }catch (ArithmeticException e){
                logger.error("User not detected"+e);
            }
        }
        if (employ.isPermission()){
            logger.info("USER : "+employ+new Date());
        }else {
            logger.warn("USER : "+employ+"Hav no permission");
        }

    }

    @Override
    public void checkOut(Employ employ) {

    }
}
