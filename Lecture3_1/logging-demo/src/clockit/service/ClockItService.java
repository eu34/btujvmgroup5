package clockit.service;

import clockit.model.Employ;

public interface ClockItService {
    void checkIn(Employ employ);
    void checkOut(Employ employ);
}
